@extends('navigation.developer')
@extends('header.developer')

<x-app-layout>
<div class="py-12 mt-5">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg mt-5">
                <div id="testing" class="p-6 bg-white border-b border-gray-200">
                    <div class="mt-row">
                        <h1 class="font-bold text-2xl text-gray-600 leading-tight mt-2">
                                {{ __('Staff Developer') }}
                        </h1>
                        <h2 class="font-light text-xl text-gray-500 leading-tight mt-2">
                                Hello, {{ Auth::user()->name }} !
                        </h2>
                    </div>
                </div>              
            </div>
        </div>
</div>
<div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <div id="testing" class="p-6 bg-white border-b border-gray-200">
                    <div class="mt-row">
                        <h1 class="font-bold text-2xl text-gray-600 leading-tight mt-2">
                                {{ __('Staff Developer') }}
                        </h1>
                        <h2 class="font-light text-xl text-gray-500 leading-tight mt-2">
                                Hello, {{ Auth::user()->name }} !
                        </h2>
                    </div>
                </div>              
            </div>
        </div>
</div>
</x-app-layout>

