<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\facades\Auth;


class UserLoginController extends Controller
{
    public function index()
    {
        $role=Auth::user()->role;

        if ($role=='Staf HRD')
        {
            return view('hrd.dashboard');
        }
        else if($role=='Staf Karyawan'){
            return view('karyawan.dashboard');
        }
        else if($role=='Staf Petinggi'){
            return view('petinggi.dashboard');
        }
        else{
            return view('developer.dashboard');
        }
    }
    public function ___construct(){
        $this->middleware(['auth','verified']);
    }
}
