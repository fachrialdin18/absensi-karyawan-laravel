<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserLoginController;
use App\Http\Controllers\AbsensiKaryawan;
use Illuminate\Support\Facades\Auth;




/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
])->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
});


//Akses Staff HRD
Route::middleware(['auth:sanctum', 'verified'])->get('/staff-hrd-dashboard', function () {
    return view('hrd.dashboard');
})->name('hrd.dashboard');

//Akses Staff Karyawan
Route::middleware(['auth:sanctum', 'verified'])->get('/staff-karyawan-dashboard', function () {
    return view('karyawan.dashboard');
})->name('karyawan.dashboard');

//Akses Staff Petinggi
Route::middleware(['auth:sanctum', 'verified'])->get('/staff-petinggi', function () {
    return view('petinggi.dashboard');
})->name('petinggi.dashboard');

//Akses Developer Staff
Route::middleware(['auth:sanctum', 'verified'])->get('/staff-developer', function () {
    return view('developer.dashboard');
})->name('developer.dashboard');

Route::resource('buat-absensi', AbsensiKaryawan::class, ['except' => [
    'create', 'update','show'
]]);


//Multiuser Login
Route::get('redirects','App\Http\Controllers\UserLoginController@index')->middleware(['auth','verified']);

