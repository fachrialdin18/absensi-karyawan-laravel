<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('main_absens', function (Blueprint $table) {
            $table->id('id_absensi');
            $table->foreignId('id_karyawan')->constrained('users');
            $table->date('tanggal_absen');
            $table->string('status');//absen,masuk,pending
            $table->integer('jumlah_overtime')->nullable();
            $table->string('catatan_karyawan')->nullable();
            $table->string('approval_hrd')->nullable();
            $table->string('catatan_hrd')->nullable();
            $table->string('approval_petinggi')->nullable();
            $table->string('catatan_petinggi')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('main_absens');
    }
};
